RUST_FILES=$(shell find ./src/ -iname "*.rs")
ASSETS_FILES=$(shell find ./assets/ -iname "*")

target/wasm32-unknown-unknown/release/rust-game.wasm: Cargo.toml Cargo.lock ${RUST_FILES}
	cargo build --release --target wasm32-unknown-unknown

dist: target/wasm32-unknown-unknown/release/rust-game.wasm ${ASSETS_FILES}
	wasm-bindgen --out-dir $@ --target web $<
	cp -r ./assets ./dist/assets
	cp index.html ./dist/index.html

clean:
	rm -rf dist/ target/


test:
	cargo test
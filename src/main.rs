pub const HEIGHT: f32 = 720.0;
pub const WIDTH: f32 = 1280.0;

mod space_invaders;
mod tower_defense;

fn main() {
    tower_defense::main();
    //space_invaders::main();
}

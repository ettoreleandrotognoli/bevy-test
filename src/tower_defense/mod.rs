use bevy::{pbr::NotShadowCaster, prelude::*};
use bevy_inspector_egui::WorldInspectorPlugin;

use bevy_mod_picking::*;

use crate::{HEIGHT, WIDTH};

use self::{
    bullet::{Bullet, BulletPlugin},
    target::{Target, TargetPlugin, spawn_target},
    tower::{Tower, TowerPlugin},
};

pub mod bullet;
pub mod target;
pub mod tower;

#[derive(Reflect, Default, Component, Debug)]
#[reflect(Component)]
pub struct Lifetime {
    timer: Timer,
}

#[derive(Reflect, Default, Component, Debug)]
#[reflect(Component)]
pub struct Health {
    pub value: i32,
}

pub struct TowerDefensePlugin {}

impl Plugin for TowerDefensePlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Health>()
            .register_type::<Lifetime>()
            .add_plugin(BulletPlugin {})
            .add_plugin(TowerPlugin {})
            .add_plugin(TargetPlugin {});
    }
}

fn spawn_camera(mut commands: Commands) {
    commands
        .spawn_bundle(Camera3dBundle {
            transform: Transform::from_xyz(-2., 2.5, 5.).looking_at(Vec3::ZERO, Vec3::Y),
            ..Default::default()
        })
        .insert_bundle(PickingCameraBundle::default());
}

fn spawn_basic_scene(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    game_assets: Res<GameAssets>,
) {
    let default_collider_color = materials.add(Color::rgba(0.3, 0.5, 0.3, 0.3).into());
    let selected_collider_color = materials.add(Color::rgba(0.3, 0.9, 0.3, 0.9).into());

    commands
        .spawn_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Plane { size: 5.0 })),
            material: materials.add(Color::rgb(0.3, 0.5, 0.3).into()),
            ..Default::default()
        })
        .insert(Name::new("ground"));
    commands
        .spawn_bundle(SpatialBundle::from_transform(Transform::from_xyz(
            0.0, 0.8, 0.0,
        )))
        .insert(Name::new("Tower_Base"))
        .insert(meshes.add(shape::Capsule::default().into()))
        .insert(default_collider_color.clone())
        .insert(Highlighting {
            initial: default_collider_color.clone(),
            hovered: Some(selected_collider_color.clone()),
            pressed: Some(selected_collider_color.clone()),
            selected: Some(selected_collider_color),
        })
        .insert(NotShadowCaster)
        .insert_bundle(PickableBundle::default())
        .with_children(|commands| {
            commands.spawn_bundle(SceneBundle {
                scene: game_assets.tower_base_scene.clone(),
                transform: Transform::from_xyz(0., -0.8, 0.),
                ..Default::default()
            });
        });
    //.spawn_bundle(PbrBundle {
    //    mesh: meshes.add(Mesh::from(shape::Cube { size: 1.0 })),
    //    material: materials.add(Color::rgb(0.67, 0.84, 0.92).into()),
    //    transform: Transform::from_xyz(0.0, 0.5, 0.0),
    //    ..Default::default()
    //})
    commands
        .spawn_bundle(PointLightBundle {
            point_light: PointLight {
                intensity: 1500.0,
                shadows_enabled: true,
                ..Default::default()
            },
            transform: Transform::from_xyz(4., 8., 4.),
            ..Default::default()
        })
        .insert(Name::new("Light"));
    spawn_target(&mut commands, &game_assets, Vec3::new(-2., 0.2, 1.5));
    spawn_target(&mut commands, &game_assets, Vec3::new(-4., 0.2, 1.5));
}

pub struct GameAssets {
    pub tower_base_scene: Handle<Scene>,
    pub tomato_tower_scene: Handle<Scene>,
    pub tomato_scene: Handle<Scene>,
    pub target_scene: Handle<Scene>,
}

/*
maybe use Bevy asset loader lib
*/
fn asset_loading(mut commands: Commands, assets: Res<AssetServer>) {
    commands.insert_resource(GameAssets {
        tower_base_scene: assets.load("TowerBase.glb#Scene0"),
        tomato_tower_scene: assets.load("TomatoTower.glb#Scene0"),
        tomato_scene: assets.load("Tomato.glb#Scene0"),
        target_scene: assets.load("Target.glb#Scene0"),
    });
}

/**
 * maybe use leadwing-input-manager
 */
fn camera_controls(
    keyboard: Res<Input<KeyCode>>,
    mut camera_query: Query<&mut Transform, With<Camera3d>>,
    time: Res<Time>,
) {
    let mut camera = camera_query.single_mut();
    let mut forward = camera.forward();
    forward.y = 0.0;
    forward = forward.normalize();

    let mut left = camera.left();
    left.y = 0.0;
    left = left.normalize();

    let speed = 3.0;
    let rotate_speed = 0.3;

    if keyboard.pressed(KeyCode::W) {
        camera.translation += forward * time.delta_seconds() * speed;
    }
    if keyboard.pressed(KeyCode::S) {
        camera.translation -= forward * time.delta_seconds() * speed;
    }
    if keyboard.pressed(KeyCode::A) {
        camera.translation += left * time.delta_seconds() * speed;
    }
    if keyboard.pressed(KeyCode::D) {
        camera.translation -= left * time.delta_seconds() * speed;
    }
    if keyboard.pressed(KeyCode::Q) {
        camera.rotate_axis(Vec3::Y, rotate_speed * time.delta_seconds());
    }
    if keyboard.pressed(KeyCode::E) {
        camera.rotate_axis(Vec3::Y, -rotate_speed * time.delta_seconds());
    }
}

fn what_is_selected(selection: Query<(&Name, &Selection)>) {
    for (name, selection) in &selection {
        if selection.selected() {
            info!("{}", name);
        }
    }
}

pub fn main() {
    let mut app = App::new();
    app.insert_resource(ClearColor(Color::rgb(0.2, 0.2, 0.2)))
        .insert_resource(WindowDescriptor {
            title: "Bevy Tower Defense".to_string(),
            width: WIDTH,
            height: HEIGHT,
            resizable: false,
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugins(DefaultPickingPlugins)
        .add_startup_system_to_stage(StartupStage::PreStartup, asset_loading)
        .add_plugin(TowerDefensePlugin {})
        .add_startup_system(spawn_basic_scene)
        .add_startup_system(spawn_camera)
        .add_system(camera_controls)
        .add_system(what_is_selected)
        .run();
}

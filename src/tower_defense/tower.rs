use bevy::{prelude::*, utils::FloatOrd};
use bevy_mod_picking::Selection;

use super::{target::Target, Bullet, GameAssets, Lifetime};

#[derive(Reflect, Default, Component)]
#[reflect(Component)]
pub struct Tower {
    pub shooting_timer: Timer,
    pub bullet_offset: Vec3,
}

pub fn tower_shooting(
    mut commands: Commands,
    game_assets: Res<GameAssets>,
    //mut meshes: ResMut<Assets<Mesh>>,
    //mut materials: ResMut<Assets<StandardMaterial>>,
    mut towers: Query<(Entity, &mut Tower, &GlobalTransform)>,
    targets: Query<&GlobalTransform, With<Target>>,
    time: Res<Time>,
) {
    for (entity, mut tower, transform) in &mut towers {
        tower.shooting_timer.tick(time.delta());
        if !tower.shooting_timer.just_finished() {
            continue;
        }
        let bullet_spawn = transform.translation() + tower.bullet_offset;

        let direction = targets
            .iter()
            .min_by_key(|target_transform| {
                FloatOrd(Vec3::distance(target_transform.translation(), bullet_spawn))
            })
            .map(|closest_target| closest_target.translation() - bullet_spawn);

        if let Some(direction) = direction {
            commands.entity(entity).with_children(|commands| {
                commands
                    //.spawn_bundle(PbrBundle {
                    //    mesh: meshes.add(Mesh::from(shape::Cube { size: 0.1 })),
                    //    material: materials.add(Color::rgb(0.87, 0.44, 0.42).into()),
                    //    transform: spawn_transformation,
                    //    ..Default::default()
                    //})
                    .spawn_bundle(SceneBundle {
                        scene: game_assets.tomato_scene.clone(),
                        transform: Transform::from_translation(tower.bullet_offset),
                        ..Default::default()
                    })
                    .insert(Lifetime {
                        timer: Timer::from_seconds(2., false),
                    })
                    .insert(Bullet {
                        direction,
                        speed: 2.5,
                    })
                    .insert(Name::new("Bullet"));
            });
        }
    }
}

pub struct TowerPlugin;

impl Plugin for TowerPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Tower>()
            .add_system(tower_shooting)
            .add_system(build_tower);
    }
}

pub fn build_tower(
    mut commands: Commands,
    selection: Query<(Entity, &Selection, &Transform)>,
    keyboard: Res<Input<KeyCode>>,
    assets: Res<GameAssets>,
) {
    if !keyboard.just_pressed(KeyCode::Space) {
        return;
    }
    for (entity, selection, transform) in &selection {
        if !selection.selected() {
            continue;
        }
        commands.entity(entity).despawn_recursive();
        spawn_tomato_tower(&mut commands, &assets, transform.translation);
    }
}

pub fn spawn_tomato_tower(commands: &mut Commands, assets: &GameAssets, position: Vec3) -> Entity {
    commands
        .spawn_bundle(SpatialBundle::from_transform(Transform::from_translation(
            position,
        )))
        .insert(Name::new("Tomato_Tower"))
        .insert(Tower {
            shooting_timer: Timer::from_seconds(0.5, true),
            bullet_offset: Vec3::new(0., 0.6, 0.),
        })
        .with_children(|commands| {
            commands.spawn_bundle(SceneBundle {
                scene: assets.tomato_tower_scene.clone(),
                transform: Transform::from_xyz(0., -0.8, 0.0),
                ..Default::default()
            });
        })
        .id()
}

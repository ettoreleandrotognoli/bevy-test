use bevy::prelude::*;

use super::{GameAssets, Health};

#[derive(Reflect, Default, Component)]
#[reflect(Component)]
pub struct Target {
    pub speed: f32,
}

pub fn move_targets(mut targets: Query<(&Target, &mut Transform)>, time: Res<Time>) {
    for (target, mut transform) in &mut targets {
        transform.translation.x += target.speed * time.delta_seconds();
    }
}

pub fn target_death(mut commands: Commands, targets: Query<(Entity, &Health)>) {
    for (entity, health) in &targets {
        if health.value <= 0 {
            commands.entity(entity).despawn_recursive();
        }
    }
}

pub struct TargetPlugin {}

impl Plugin for TargetPlugin {
    fn build(&self, app: &mut App) {
        app.register_type::<Target>()
            .add_system(target_death)
            .add_system(move_targets);
    }
}

pub fn spawn_target(
    commands: &mut Commands,
    game_assets: &Res<GameAssets>,
    position: Vec3,
) -> Entity {
    commands
        //.spawn_bundle(PbrBundle {
        //    mesh: meshes.add(Mesh::from(shape::Cube { size: 0.4 })),
        //    material: materials.add(Color::rgb(0.67, 0.84, 0.92).into()),
        //    transform: Transform::from_xyz(-4., 0.2, 1.5),
        //    ..Default::default()
        //})
        .spawn_bundle(SceneBundle {
            scene: game_assets.target_scene.clone(),
            transform: Transform::from_translation(position),
            ..Default::default()
        })
        .insert(Target { speed: 0.3 })
        .insert(Health { value: 3 })
        .insert(Name::new("Target"))
        .id()
}
